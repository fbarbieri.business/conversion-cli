#! /usr/bin/env node

const chalk = require('chalk');
const { string, argv } = require("yargs");
const yargs = require("yargs");
var path =  require("path");

const usage = "To use this Conversion CLI you need to use the " + chalk.green.bold('conv') + " command." +
    "\nList of availabe commands:\n" + 
    '\nThis command is used for change the units of measure of a number:' 
    + '\n ' + chalk.green.bold('conv')
    + ' ' + chalk.green.bold('u')
    + ' <' + chalk.blue.bold('nubmerToConvert') + '>'
    + ' <' + chalk.red.bold('starterUnits') + '>'
    + ' <' + chalk.red.bold('finalUnits') + '> \n' +
    '\nThis command is used for change the numeric system of a number:'
    + '\n ' + chalk.green.bold('conv')
    + ' ' + chalk.green.bold('b')
    + ' <' + chalk.blue.bold('nubmerToConvert') + '>'
    + ' <' + chalk.red.bold('starterBase') + '>'
    + ' <' + chalk.red.bold('finalBase') + '> \n' +
    '\nYou can see all supported units of measure by using this command:'
    + '\n ' + chalk.green.bold('conv list');

const options = yargs  
    .usage(usage)
    .command('list', 'Get list of all supported units.')
    .command('u', 'Change between different units of measure.')
    .command('b', 'Change between different numeric systems.')
    .help()
    .argv;

var dir = __dirname;
const fs = require('fs');
var rawdata = fs.readFileSync(dir + '/ulist.json');
var data = JSON.parse(rawdata);

function checkUParams (number)
{
    if(
        typeof number !== 'undefined'
        && number.toString().match('^\\d+(\\.\\d+)*$')
    )
        return true;
    else
        return false;
}

function print(type, arguments)
{
    switch(type)
    {
        case 'uprint':
            console.log(
                arguments[0]
                + ' ['
                + chalk.red.bold(arguments[1])
                + '] '
                + chalk.green.bold('-> ')
                + arguments[2]
                + ' ['
                + chalk.red.bold(arguments[3])
                +']');
            break;
        case 'bprint':
            console.log(
                arguments[0]
                + ' ('
                + chalk.red.bold(arguments[1])
                + ') '
                + chalk.green.bold('-> ')
                + arguments[2]
                + ' ('
                + chalk.red.bold(arguments[3])
                +')');
            break;
        default:
            console.log(usage)
            break;
    }
}

function changeTemps (number, currentUnits, desiredUnits)
{
    switch (currentUnits)
    {
        case 'F':
            if(desiredUnits == 'C')
                return ((number - 32) / 1.8);
            else if (desiredUnits == 'K')
                return ((number - 32) / (1.8 + 273.15));
            else
                return number;
            break;
        case 'C':
            if(desiredUnits == 'F')
                return ((number * 1.8) + 32);
            else if (desiredUnits == 'K')
                return (number + 273.15);
            else
                return number;
            break;
        case 'K':
            if(desiredUnits == 'F')
                return ((number - 273.15) * 1.8 + 32);
            else if (desiredUnits == 'C')
                return (number - 273.15);
            else
                return number;
            break;
        default:
            // throw exeption
            break;
    }
}

// Main process

switch (argv._[0])
{
    case 'list':
        console.log(chalk.green.bold('List of all supported units:'))
        
        for(var item in data)
        {
            console.log(
                ' - ['
                + chalk.red.bold(item)
                + ']  \t  -> '
                + data[item]['name']
                + '\t \t'
                + data[item]['type']);
        }

        break;

    case 'u':
        if (checkUParams(argv._[1]))
        {
            var number = argv._[1];
            var currentUnits = argv._[2];
            var desiredUnits = argv._[3];

            var output = 0;
            var cs = data[currentUnits]['value'];
            var cd = data[desiredUnits]['value'];

            if(data[currentUnits]['type'] == data[desiredUnits]['type'])
            {
                if(data[currentUnits]['type'] == 'temperature')
                {
                    output = changeTemps(number, currentUnits, desiredUnits);
                    var out = [number, currentUnits, output, desiredUnits];
                }
                else
                {
                    output = number * (cs / cd);
                    var out = [number, currentUnits, output, desiredUnits];
                }
                print('uprint', out)
            }
            else
            {
                console.log(chalk.yellow('Warning:') + ' Operation between not homogeneous units.');
            }

            

            // raggruppare il tutto in funzioni, creare un try and catch e suddividere con uno switch le varie operazioni
            // divise per tipo (speed, temperature, etc). Quello che cambia è la formula da applicare per calcolare l'output
            
            var out = [number, currentUnits, output, desiredUnits];
            // print ('uprint', out);
        }
        else
        {
            console.log(usage)
        }
        break;
    case 'b':

        // si potrebbero implementare con delle opzioni il numero espresso in ca2, ca1, MS, virgola mobile etc 

        var number = argv._[1].toString();
        var startingSystem = argv._[2];
        var desiredSystem = argv._[3];

        if(startingSystem > 36 || desiredSystem > 36)
        {
            console.log(chalk.bgRed('Error:') + ' Both numeral system must be included between 2 and 36.')
            break;
        }

        var temp = parseInt(number, startingSystem)
        var toPrint = temp.toString(desiredSystem)

        var out = [number, startingSystem, toPrint, desiredSystem];
        print ('bprint', out);

        break;
    default:
        console.log(usage)
        break;
}