# Conversion CLI

## Introduction

This CLI was made mainly for fun. Secondly, it came in handy for checking conversions while preparing for computer physics and computer architecture exams at my University.

## How to install

If you want to install the conversion CLI globally run this command in your terminal.

    npm install -g conversioncli@1.0.0

Remember: you also need npm and node.js already installed.

## Commands

This CLI can perform 2 different type of operations:

- Base change operations (numeric system)
- Convert from one unit of measurement to another (equivalences)

### Change numeric system

To change the numeric system of a number use this command. Each element is mandatory.

```
conv b <number_to_convert> <starting_numeric_system> <final_numeric_system>
```

For example, if you want to convert 5 from base 10 to bynary:

```
conv b 5 10 2
```

Output:

```
5 (10) -> 101 (2)
```

### Change units of measurement

To convert the units of measurement use this command. Each element is mandatory.

```
conv u <number_to_convert> <starting_units> <final_units>
```

For example, if you want to convert 5 kilos to grams:

```
conv u 5 kg g
```

Output:

```
5 [kg] -> 5000 [g]
```

If you try to convert not homogeneous units you will get a warning.

### Other commands

If you want to see all supported unit of measurement you can use this command.

```
conv list
```

If you need more information about this CLI you can use this command.

```
conv
```

## How to uninstall

If you want to uninstall the conversion CLI run this command in your terminal.

    npm uninstall -g conversioncli

Remember: you need to put the "-g" only if you installed this package globally.

## Changelog

    1.0.0 - First version of the CLI, just for testing purpose, still quite buggy and with some errors :c
